const { Kafka } = require("kafkajs");

const kafka = new Kafka({
    clientId: 'kafka-node-demo',
    brokers: ['localhost:9092']
});

const producer = kafka.producer();
const consumer = kafka.consumer({ groupId: 'test-group' });

const run = async () => {
    // Produce
    await producer.connect();
    await producer.send({
        topic: 'test-topic',
        messages: [
            { value: 'This is the first message' },
            { value: 'This is the second message' }
        ]
    });

    console.log('At this point, messages are being stored in the broker.');

    // Consume
    await consumer.connect();
    await consumer.subscribe({ topic: 'test-topic', 'fromBeginning': false });
    await consumer.run({
        eachMessage: async ({ topic, partition, message }) => {
            console.log({
                partition,
                offset: message.offset,
                value: message.value.toString()
            });
        }
    });

    // Produce second batch
    await producer.send({
        topic: 'test-topic',
        messages: [
            { value: 'This is the third message' },
            { value: 'This is the fourth message' }
        ]
    });
};

run().catch(console.error());